# Quasi: An extensible, user friendly multiphysics program

*Quasi* is a multiphysics differential equation solver, and simulation
program that will be written in [Julia][jl]. Its main goal is to be easy
enough for anyone to pick up and use, and feature-rich enough in order to
be utilized by academic, and industrial users.

[jl]: julialang.org/

In order to achieve this, Quasi will use three layers, each getting
closer to "bare metal" source code:

1. User input --- files, interactive prompts, and GUIs that specify
parameters and objects to be solved;

2. Model scripting --- scripts that provide assumptions and equations to
a solver;

3. Solver programming --- programs written in Julia that solve
equations---while taking into consideration assumptions---written in the
model script.

All of these will be packaged neatly into either a GUI, or LUI.

The reason behind this seemingly contrived system is to implement a sort
of faux-Unix philosophy. Each file can be short, self-contained, and easy
to modify, throw out, or create.

Originally, I was planning on writing this in Haskell, since I thought
"hey, it's functional programming! That means it should be great for
mathematical routines," without realizing that, while Haskell can certainly
do math, it's probably better to do it with a language that's more
optimized for it. So, I instead chose to write Quasi in [Julia][jl]. I
will be doing my best in order to avoid using external libraries, as I want
Quasi to be an independent system that won't suddenly break if there's a
big update.

## Dependencies

* [Julia][jl]

* It hasn't been written yet---not even a prototype is ready. Get outta
here!

## License summary

The GNU Affero General Public License (AGPL) allows four fundamental
freedoms: to run this program however you want; to study how the program
works, and make changes accordingly; to distribute the unmodified program
to others; to share any modified versions of the program with other people.
Because this program is licensed under the AGPL, any programs written for,
from, or utilizing Quasi must also be licensed under the AGPL, and include
a distribution of Quasi's source code. In addition, since Quasi is licensed
under the AGPL, any version of it---modified, or unmodified---allows for
its source code to be downloaded over any network.

